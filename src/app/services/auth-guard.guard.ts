import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { CanDeactivate } from '@angular/router';
import { WebcamComponent } from 'ngx-webcam';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate,CanLoad,CanDeactivate<WebcamComponent> {
constructor(private router:Router){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      // this.router.navigate(['login']);
      return true;
  }
  
  canLoad(): boolean {
    return true;
  }
  canDeactivate(component:WebcamComponent){
    component.ngOnDestroy();
    return true;
  }
  
}
