import { Injectable } from "@angular/core";
import { HttpClient,HttpHeaders  } from '@angular/common/http';
import { Observable } from "rxjs";
import { map, catchError, filter } from 'rxjs/operators';

@Injectable({
    providedIn:'root'
})

export class apiService {
    constructor(private http:HttpClient){}
   
    public myData:string="";
    public status:boolean =false;
    baseUrl="https://jsonplaceholder.typicode.com/posts";

    getUser():Observable<any>{
        return this.http.get(this.baseUrl)
        .pipe(
            map((el:any) => el.slice(0, 10))); 
    }
    getCustomer(){
        return this.http.get('http://localhost:3000/sales')
    }
        
    

}
