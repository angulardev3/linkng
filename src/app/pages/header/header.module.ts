import { Injectable, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes,RouterModule, Router } from '@angular/router';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { ReactiveformComponent } from '../reactiveform/reactiveform.component';
import { ProductsComponent } from '../products/products.component';
import { HeaderComponent } from './header.component';
import { UserComponent } from '../user/user.component';
import { WebcamComponent } from '../webcam/webcam.component';
import { WebCamModule } from '../webcam/webcam.module';
import { WebcamModule } from 'ngx-webcam';
import { AuthGuard } from 'src/app/services/auth-guard.guard';
import { TemplateformComponent } from '../templateform/templateform.component';
import { FormsModule } from '@angular/forms';



const routes:Routes=[
  {path:"",pathMatch:"full",component:DashboardComponent},
  {path:"dashboard",component:DashboardComponent},
  {path:"reactiveform",component:ReactiveformComponent},
  {path:"products",component:ProductsComponent},
  {path:"templateForm",component:TemplateformComponent},
  {path:"user",component:UserComponent},
  {path:"webcam",
  canDeactivate:[AuthGuard],
  component:WebcamComponent},
]
@NgModule({
  declarations: [
  HeaderComponent,
  DashboardComponent,
  ReactiveformComponent,
  ProductsComponent,
  WebcamComponent,
  UserComponent,
  TemplateformComponent
],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    WebcamModule,
    FormsModule,  
  ],
  
})
export class HeaderModule { }
