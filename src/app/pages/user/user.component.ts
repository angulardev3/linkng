import { Component, OnInit,NgZone } from '@angular/core';
import { apiService } from 'src/app/services/api.service';
import { User } from 'src/app/user';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
 users:any;
  constructor(private apiService:apiService, private ngZone:NgZone) { }

  ngOnInit(): void {
    this.apiService.getUser().subscribe(data=>{
      this.users=data;
      console.log("this is data",this.users);

    })
  }
  userDetails(id: number) {
    console.log(id);
  }
  removeUser(id:number){
    console.log(id)
  }
}
