import { Component, OnInit,NgZone } from '@angular/core';
import { apiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  constructor(private apiService:apiService,private ngZone:NgZone) { }
  data=""
  ngOnInit(): void {
    this.ngZone.runOutsideAngular(()=>{
      setTimeout(()=>{
        console.log("i will check");
      },1000)
    })
    // this.data=this.apiService.myData  
  }

}
