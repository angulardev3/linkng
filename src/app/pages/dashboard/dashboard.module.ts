import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module';
import { DashboardComponent } from './dashboard.component';
import { Routes,RouterModule } from '@angular/router';

const route:Routes=[{
  path:"/dashboard",
  component:DashboardComponent},
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(route),
  ],
})
export class DashboardModule { }
