import { Component, OnInit } from '@angular/core';
import {apiService} from '../../services/api.service';
import {Chart,registerables} from 'node_modules/chart.js';
Chart.register(...registerables);
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private apiService:apiService) { }
  typedtext:string="";
  generatAot:string="Ahead of Time";
  title='ng-chart';
  chart:any=[];
  labeldata:any[]=[];
  realdata:any[]=[];
  colordata:any[]=[];
  chartdata:any;
  // public btnClick():void{
  //   this.apiService.myData="This is parent data"
  // }
  ngOnInit(){
    this.apiService.getCustomer().subscribe((res)=>{
      this.chartdata=res;
      if(this.chartdata!=null){
        for(let i=0;i<this.chartdata.length;i++){
          console.log(this.chartdata[i]);
          this.labeldata.push(this.chartdata[i].year);
          this.realdata.push(this.chartdata[i].amount);
          this.colordata.push(this.chartdata[i].colorcode);
        }
      }
      this.renderData(this.labeldata,this.realdata,this.colordata,'pie','piechart');
      this.renderData(this.labeldata,this.realdata,this.colordata,'bar','barchart');
      this.renderData(this.labeldata,this.realdata,this.colordata,'doughnut','doughnutchart');
      this.renderData(this.labeldata,this.realdata,this.colordata,'bubble','bubblechart');

    })
  }
  renderData(labeldata:any,maindata:any,colordata:any,type:any,id:any){
   this.chart = new Chart(id, {
      type: type,
      data: {
        labels: labeldata,
        datasets: [
          {
            label: 'amount',
            data: maindata,
            backgroundColor:colordata,
            borderWidth: 1,
          },
        ],
      },
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
    });
  }
  

}
