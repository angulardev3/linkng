import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { FormGroup,FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm:any=FormGroup;
  formData:string="";
  constructor(private router:Router) { }

  ngOnInit(): void {
    this.registerForm=new FormGroup({
      firstName:new FormControl("",[Validators.required,Validators.minLength(2)]),
      lastName:new FormControl("",[Validators.required]),
      email:new FormControl("",[Validators.required]),
      password:new FormControl("",[Validators.required,Validators.minLength(6),Validators.maxLength(16)]),
      agree:new FormControl(false,[Validators.requiredTrue])
    })
  }

  onSubmit(){
  const formValue=this.registerForm.value;
  localStorage.setItem('formData',JSON.stringify(formValue));  
  const formData = JSON.parse(localStorage.getItem('formData') || '{}');
    console.log(formData)
  this.router.navigate(['./login'])
  }

  
}
