import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm:any=FormGroup
  constructor(private router:Router) { }

  ngOnInit(): void {
    this.loginForm=new FormGroup({
      email:new FormControl("",[Validators.required]),
      password:new FormControl("",[Validators.required,Validators.minLength(6)])
    })
  }
  onSubmit(){
    const loginFormData=this.loginForm.value;
    localStorage.setItem('loginData',JSON.stringify(loginFormData));
    this.router.navigateByUrl('/dashboard');
  }
}
