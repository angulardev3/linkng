import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { WebcamComponent } from './webcam.component';
import { RouterModule, Routes } from '@angular/router';

const routes:Routes=[{
    path:"webcam",component:WebcamComponent
}]
@NgModule({
    declarations:[],
    imports:[
        BrowserModule,
        RouterModule.forChild(routes)]
})
export class WebCamModule{}