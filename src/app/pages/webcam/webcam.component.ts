import { Component, OnDestroy, OnInit } from '@angular/core';
import { WebcamImage } from 'ngx-webcam';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-webcam',
  templateUrl: './webcam.component.html',
  styleUrls: ['./webcam.component.scss']
})
export class WebcamComponent implements OnInit,OnDestroy {
  stream:any=null;
  status:any=null;
  trigger:Subject<void>=new Subject();
  previewImage:string="";
  btnLabel="Capture Image"
  constructor() { }

  ngOnInit(): void {
  }
  get $trigger():Observable<void>{
    return this.trigger.asObservable();
  }
  checkPermission(){
    navigator.mediaDevices.getUserMedia({
      video:{
        width:500,
        height:500,
      }
    }).then((response)=>{
      this.stream=response;
      this.status="My camera is accessing";
      this.btnLabel="Capture Image"
    }).catch(err=>{
      this.status=err
      if(err?.message==="permission denied"){
        this.status="persmission denied please try again"
      }else{
        this.status="You may not have camera system"
      }
    })
  }
  snapShot(event:WebcamImage){
    console.log(event);
    this.previewImage=event.imageAsDataUrl;
    this.btnLabel="Recapture Image";
    const data=event.imageAsDataUrl
   
    const blob = new Blob([data], { type: 'image/jpeg' });
    const fileName = 'image.jpg';
  
  }
  captureImage(){
    this.trigger.next();
  }
  ngOnDestroy(){
    this.trigger.unsubscribe();
    console.log("Camera closed")
  }
}
