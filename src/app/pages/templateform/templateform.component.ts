import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-templateform',
  templateUrl: './templateform.component.html',
  styleUrls: ['./templateform.component.scss']
})
export class TemplateformComponent implements OnInit {
  countryList=[
    {id:1,name:"India"},
    {id:2,name:"USA"},
    {id:3,name:"England"}
  ]
  constructor() { }

  ngOnInit(): void {
  }
  formSubmit(signUp:any){
    console.log("form submitted",signUp.value)
  }
}
