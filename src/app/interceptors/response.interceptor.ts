import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable()
export class ResponseInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const startTime = (new Date()).getTime();
    console.log(startTime);
    return next.handle(request).pipe(
      map(event => {
        if (event instanceof HttpResponse) {
          console.log(event);
          const endTime = (new Date).getTime();
          
          const responseTime = endTime - startTime;
          console.log(`${event.url} Hi Manoj the response succeed in ${responseTime}`)

        }
        return event;
      }))
  }
}
