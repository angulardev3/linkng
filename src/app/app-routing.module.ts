import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';


const routes: Routes = [
  {path:"",redirectTo:"login",pathMatch:'full'},
  {path:"login", component:LoginComponent},
  {path:"register",component:RegisterComponent},
  {path:"header",loadChildren:()=>import('./pages/header/header.module').then(m=>m.HeaderModule)},
  {path:"",loadChildren:()=>import('./pages/dashboard/dashboard.module').then(m=>m.DashboardModule)},
  {path:"**", redirectTo:"/404"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
