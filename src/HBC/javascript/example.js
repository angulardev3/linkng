let obj={
    name:"manoj",
    age:20
}
obj='key';
console.log( obj)
// const myfn=async()=>{
//     try {
//         const response = await fetch('https://jsonplaceholder.typicode.com/todos/1');
//                 const json = await response.json();
//                 // console.log("json data",json) 
//     } catch (error) {
//         console.log(error)
//     }
// }
// myfn();
//IIFE
// (async function(){
//     try {
//         const response = await fetch('https://jsonplaceholder.typicode.com/todos/1');
//         const json = await response.json();
//         console.log("json data",json)   
//     } catch (error) {
//         console.log("json data",error)   

//     }
    
// })()
// async function runProcess() {
//     const response = await fetch('https://jsonplaceholder.typicode.com/todos/1');
//     const json = await response.json();
//     console.log(json)
//   }
//   runProcess();

//callback
// function parentfn(add){
//     console.log(add);

// }
// function addFn(a,b,parentfn){
//     let c= a+b;
//     parentfn(c)
// }
// addFn(2,4,parentfn)
// async function myFn(){
//     let mypromise=new Promise((resolved,rejected)=>{
//     const a=2;
//     const b=4;
//     const add=a+b;
//     if(add===6){
//         resolved(`yes ${a} + ${b}=4`)
//     }else{
//         rejected(`no ${a}+${b} !=4`)
//     }
// })
// await mypromise.then((m)=>{
//     console.log("success",m)
// }).catch((err)=>{
//     console.log("Error",err)
// })
// }
 
// myFn();